# ArcanysAlphaLoggerBundle
### Composer

```json
"require": {
        "arc_robel/arcanys-alpha-logger-bundle":"1.0.*",
        "doctrine/mongodb-odm": "1.0.*@dev",
        "doctrine/mongodb-odm-bundle": "3.0.*@dev",
        "stof/doctrine-extensions-bundle": "1.1.*@dev"
    }, 
"repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:arc_robel/arcanys-alpha-logger-bundle.git"
        }
    ],
```
### Required Configuration
```yaml
doctrine_mongodb:
    connections:
        default:
            server: %arcanys.alphalogger.mongo.server%
            options: {}
    default_database: %arcanys.alphalogger.mongo.defaultdb%
    document_managers:
        default:
            auto_mapping: true
            
stof_doctrine_extensions:
    default_locale: en_US
    mongodb:
        default:
            timestampable: true
```
### Required Parameters
```yaml
arcanys.alphalogger.mongo.server: <mongodb://localhost:27017>
arcanys.alphalogger.mongo.defaultdb: <mdb>
```

### AppKernel register bundles
```php
$bundles = array(
            new Arcanys\AlphaLoggerBundle\ArcanysAlphaLoggerBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
);
```
### Basic Use
```php
$log = $this->get('arcanys_alpha_logger.service')->createLog($accountId,$action,$data);
```
### Basic Usage Example
```php
$accountId = $this->user()->getId();
$action = 'Contact Create';
$data = ['firstName' => 'Ian'];
$log = $this->get('arcanys_alpha_logger.service')->createLog($accountId,$action,$data);
```