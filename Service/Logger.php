<?php

namespace Arcanys\AlphaLoggerBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Arcanys\AlphaLoggerBundle\Document\ActivityLog;

class Logger
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createLog($accountId, $action, $data = array())
    {
        $log = new ActivityLog();
        $log->setAccountId($accountId);
        $log->setActivityType($action);
        $log->setData($data);
        $dm = $this->container->get('doctrine_mongodb')->getManager();
        $dm->persist($log);
        $dm->flush();
        return $log;
    }

}
